# -*- coding: utf-8 -*-

from uuid import UUID
from typing import Optional
from pydantic import BaseModel, EmailStr


class Token(BaseModel):
    access_token: str
    token_type: str
