# -*- coding: utf-8 -*-

from datetime import timedelta
from typing import Any, List
from fastapi import status
from sqlalchemy.orm import Session
from fastapi import APIRouter, Body, Depends, HTTPException
from app import models, schemas
from fastapi.security import OAuth2PasswordRequestForm
from app.services.services.service_user import UserService
from app.core import security
from app.api import deps
from app.utils.service_result import handle_result
from app.core.config import settings


router = APIRouter()


@router.post("/", name="login:login-user", response_model=schemas.Token)
def login_access_token(db: Session = Depends(deps.get_db), form_data: OAuth2PasswordRequestForm = Depends()) -> Any:
    """
    login user by email and password.
    """
    user = UserService(db).authenticate_user(email=form_data.username, password=form_data.password)
    if not user.success:
        return handle_result(user)
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    return {
        "access_token": security.create_access_token(user.value.id, expires_delta=access_token_expires),
        "token_type": "bearer",
    }
