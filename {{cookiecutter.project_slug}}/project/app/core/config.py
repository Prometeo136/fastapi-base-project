# -*- coding: utf-8 -*-

import os
from starlette.datastructures import Secret
from typing import Any, Dict
from pydantic import BaseSettings


class Settings(BaseSettings):
    VERSION: str = "1.0.0"
    PROJECT_NAME: str = "{{ cookiecutter.project_slug }}"
    VERSION = "1.0.0"
    API_PREFIX = "/api"
    SECRET_KEY = os.getenv("SECRET_KEY")
    POSTGRES_USER = os.getenv("POSTGRES_USER")
    POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
    POSTGRES_HOST = os.getenv("POSTGRES_HOST")
    POSTGRES_PORT = os.getenv("POSTGRES_PORT")
    POSTGRES_DB = os.getenv("POSTGRES_DB")
    DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}"
    # 60 minutes * 24 hours * 10 days = 10 days
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 10


settings = Settings()
