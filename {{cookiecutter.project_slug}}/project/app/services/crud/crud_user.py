# -*- coding: utf-8 -*-

from typing import Any, Dict, Optional, Union
from app.services.main import AppCRUD
from app.core.security import get_password_hash, verify_password
from app.models.users import Users
from app.services.crud.base import CRUDBase
from app.schemas.user import UserCreate, UserUpdate


class CRUDUser(CRUDBase[Users, UserCreate, UserUpdate]):
    def get_by_email(self, db: AppCRUD, *, email: str) -> Optional[Users]:
        """
        Search one user in the database by email.
        """
        return db.query(Users).filter(Users.email == email).first()

    def get_multi(self, db: AppCRUD, *, skip: int = 0, limit: int = 100) -> Optional[Users]:
        """
        List of users paginated.
        """
        return db.query(Users).filter(Users.is_active.is_(True)).offset(skip).limit(limit).all()

    def create(self, db: AppCRUD, *, obj_in: UserCreate) -> Users:
        """
        Insert one user in the database.
        """
        db_obj = Users(
            email=obj_in.email,
            hashed_password=get_password_hash(obj_in.password),
            is_superuser=obj_in.is_superuser,
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        return db_obj

    def update(self, db: AppCRUD, *, db_obj: Users, obj_in: Union[UserUpdate, Dict[str, Any]]) -> Users:
        """
        Update user information.
        """
        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)
        if update_data.get("password"):
            hashed_password = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password
        return super().update(db, db_obj=db_obj, obj_in=update_data)

    def authenticate(self, db: AppCRUD, *, email: str, password: str) -> Optional[Users]:
        """
        Authenticate one user by its email and password.
        """
        user = self.get_by_email(db, email=email)
        if not user:
            return None
        if not verify_password(password, user.hashed_password):
            return None
        return user

    def delete(self, db: AppCRUD, *, id: str) -> Users:
        """
        Delete one user.
        """
        user = db.query(Users).filter(Users.id == id).first()
        user.is_active = False
        db.commit()
        db.refresh(user)
        return user


user = CRUDUser(Users)
