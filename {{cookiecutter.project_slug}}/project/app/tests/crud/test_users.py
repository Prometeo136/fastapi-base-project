# -*- coding: utf-8 -*-

from app.core.security import get_password_hash
from sqlalchemy.orm import Session
from app.services import crud
from app.models.users import Users
from app.schemas.user import User, UserCreate, UserUpdate
from app.tests.factories.user import factory_create_user


class TestUserBasicCrud:
    def test_create_user(self, db: Session) -> None:
        email = "email@gmail.com"
        password = "password"
        is_active = True
        is_superuser = False
        user_in = UserCreate(email=email, password=password, is_active=is_active, is_superuser=is_superuser)
        user = crud.user.create(db, obj_in=user_in)
        assert user.email == email
        assert hasattr(user, "hashed_password")

    def test_get_user_by_id(self, db: Session) -> None:
        user = factory_create_user(db, size=1)
        retrieved_user = crud.user.get(db, id=user.id)
        assert retrieved_user

    def test_get_list_of_users(self, db: Session) -> None:
        factory_create_user(db, size=10)
        retrieved_user = crud.user.get_multi(db)
        assert len(retrieved_user) == 10

    def test_get_user_by_email(self, db: Session) -> None:
        user = factory_create_user(db)
        retrieved_user = crud.user.get_by_email(db, email=user.email)
        assert retrieved_user

    def test_update_user(self, db: Session) -> None:
        email = "email@gmail.com"
        password = "password"
        is_active = True
        is_superuser = False
        user_in = UserCreate(email=email, password=password, is_active=is_active, is_superuser=is_superuser)
        user = crud.user.create(db, obj_in=user_in)
        user_update_in = UserUpdate(is_active=False, is_superuser=True)
        user_updated = crud.user.update(db, db_obj=user, obj_in=user_update_in)
        assert not user_updated.is_active
        assert user_updated.is_superuser

    def test_authenticate_user(self, db: Session) -> None:
        password = "test"
        hashed_password = get_password_hash("test")
        user = factory_create_user(db, size=1, hashed_password=hashed_password)
        authenticate = crud.user.authenticate(db, email=user.email, password=password)
        assert authenticate

    def test_delete_user(self, db: Session) -> None:
        user = factory_create_user(db, is_active=True)
        crud.user.delete(db, id=user.id)
        deleted_user = db.query(Users).filter(Users.id == user.id).first()
        assert not deleted_user.is_active
