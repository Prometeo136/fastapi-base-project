# -*- coding: utf-8 -*-

import os
import pytest
from sqlalchemy.orm import Session
from starlette.testclient import TestClient
from starlette.status import HTTP_200_OK, HTTP_401_UNAUTHORIZED
from app.main import app
from app.core.security import get_password_hash
from app.models.users import Users
from app.tests.factories.user import factory_create_user


class TestLoginUser:
    def test_correct_login(self, db: Session, client: TestClient) -> None:
        password = "password"
        hashed_password = get_password_hash(password)
        user = factory_create_user(db, hashed_password=hashed_password)
        body = {"username": user.email, "password": password}
        response = client.post(app.url_path_for("login:login-user"), data=body)
        assert response.status_code == HTTP_200_OK
        assert response.json().get("access_token")

    def test_incorrect_password(self, db: Session, client: TestClient) -> None:
        password = "password"
        hashed_password = get_password_hash(password)
        user = factory_create_user(db, hashed_password=hashed_password)
        body = {"username": user.email, "password": "testing"}
        response = client.post(app.url_path_for("login:login-user"), data=body)
        assert response.status_code == HTTP_401_UNAUTHORIZED

    def test_login_inactive_user(self, db: Session, client: TestClient) -> None:
        password = "password"
        hashed_password = get_password_hash(password)
        user = factory_create_user(db, hashed_password=hashed_password, is_active=False)
        body = {"username": user.email, "password": password}
        response = client.post(app.url_path_for("login:login-user"), data=body)
        assert response.status_code == HTTP_401_UNAUTHORIZED
