# -*- coding: utf-8 -*-

import alembic
import pytest
from starlette.testclient import TestClient
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy_utils import create_database, database_exists, drop_database
from fastapi import FastAPI
from app.db.base_class import Base
from alembic.config import Config
from app.main import get_application, app
from app.api.deps import get_db
from app.core.config import settings


url = f"{settings.DATABASE_URL}_test"
_db_conn = create_engine(url)


def get_test_db_conn():
    assert _db_conn is not None
    return _db_conn


def get_test_db():
    session = Session(bind=_db_conn)

    try:
        yield session
    finally:
        session.close()


@pytest.fixture(scope="session", autouse=True)
def database():
    if database_exists(url):
        drop_database(url)
    create_database(url)
    Base.metadata.create_all(_db_conn)
    app.dependency_overrides[get_db] = get_test_db
    yield
    drop_database(url)


@pytest.yield_fixture
def db():
    db_session = Session(bind=_db_conn)

    yield db_session
    for tbl in reversed(Base.metadata.sorted_tables):
        _db_conn.execute(tbl.delete())
    db_session.close()


@pytest.fixture
def client():
    with TestClient(app) as client:
        yield client
