# {{ cookiecutter.project_name }}

{%- if cookiecutter.open_source_license != "Not open source" %}
| License     | {{cookiecutter.open_source_license}} |
| ----------- | ------------------------------------ |
{%- endif %}

# Basic commands

## Initialize the project
To initialize the project type this command in the terminal:
 ```sh
make setup
```

## Run the app
To run the app type this command in the terminal:
 ```sh
make start
```

## Run tests
To run the test type this command in the terminal:
 ```sh
make tests
```

## Run migrations
To run the test type this command in the terminal:
 ```sh
make migrations migration_message={migration_description}
```

## List all the available commands
To list all the available commands type in the terminal:
 ```sh
 make help
  ```
