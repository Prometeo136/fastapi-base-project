alembic==1.7.3
bcrypt==3.2.0
databases[postgresql]==0.5.3
email-validator==1.1.1
factory-boy==3.2.1
fastapi==0.68.1
greenlet==1.1.2
Mako==1.1.5
MarkupSafe==2.0.1
passlib==1.7.4
psycopg2-binary==2.8.6
pydantic==1.8.2
python-jose[cryptography]
python-multipart==0.0.5
requests==2.26.0
SQLAlchemy==1.4.25
SQLAlchemy-Utils==0.37.9
starlette==0.14.2
typing-extensions==3.10.0.2
uvicorn==0.15.0
