# FastAPI and PostgreSQL project generator
* This is a FastAPI project generator
* To make this project I took some ideas from the following resources:
  * https://www.jeffastor.com/blog/testing-fastapi-endpoints-with-docker-and-pytest
  * https://github.com/tiangolo/full-stack-fastapi-postgresql
  * https://camillovisini.com/article/abstracting-fastapi-services
